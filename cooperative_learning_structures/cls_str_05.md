---
Structure: 05
Content: 30 for tea
Initials: NISI
---

# 30 for tea

Knowledge about a one or more subjects or questions has to be shared

1. The first Team presents their answer. The answer is written on the white board

2. The second Team presents their answer. The answer is written on the white board

3. Continue with all Teams until either time is up, or there are no more answers

4. If needed answers are discussed on class

5. Each Team debates if their original answer should be modified

* **It is a rapid and effecient way to share knowledge between Groups**
* **The lecturer can give feedback**