---
title: 'Cooperative Learning Structures'
subtitle: 'UCL IT-Technology'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: 'UCL IT-Technology, cooperative learning structures'
---


Introduction
====================

This document is a collection of cooperative learning structures. It is based on the learning structures found in Kagan, S. & Stenlev, J. (2006). Cooperative Learning. Undervisning med samarbejdsstrukturer. København: Malling Beck.