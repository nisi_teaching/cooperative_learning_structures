---
Structure: 01
Content: Circle of knowledge
Initials: NISI
---

# Circle of Knowledge

1. A subject or assignment with multiple answers needs to be processed.
2. Team members takes turns, each giving their answers.

**The circle of knowledge is a simple way to ensure that everyone in a Team participates with answers or ideas**